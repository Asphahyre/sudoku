/*
** sudoku.c for SUDOKU in /home/bougon_p/rendu/sudoki-bi
**
** Made by bougon_p
** Login   <bougon_p@epitech.net>
**
** Started on  Fri Feb 26 21:49:12 2016 bougon_p
** Last update Sun Feb 28 08:45:41 2016 Luka Boulagnon
*/

#include "sudo.h"

int	*convert_tab(char *tab_game)
{
  int	*tab;
  int	i;
  int	p;

  if ((tab = malloc(sizeof(int) * 81)) == NULL)
    return (NULL);
  i = -1;
  p = 0;
  while (tab_game[++i] != 0)
    {
      if (tab_game[i] != '|' && tab_game[i] != '-')
	{
	  if (tab_game[i] == ' ')
	    tab[p] = 0;
	  else
	    tab[p] = tab_game[i] - '0';
	  p++;
	}
    }
  return (tab);
}

t_tab	*stock(int j, char *tab_game, t_arglist *arg)
{
  t_tab	*tab;

  if (j % 11 == 10)
    {
      if ((tab = malloc(sizeof(t_tab))) == NULL)
	return (NULL);
      tab->tab = convert_tab(tab_game);
      if (j == 10)
	create_cdlist(arg, tab);
      else
	add_last_cdl(arg, tab);
      return (tab);
    }
  return (NULL);
}

int	read_the_file(t_arglist *arg, char *line, char *tab_game, int j)
{
  while ((line = get_next_line(0)) != NULL)
    {
      j++;
      if (check_line(line, j) == 1)
	return (my_putstr_err("MAP ERROR\n\n"), 1);
      line = clean_str(line);
      tab_game = strcat(tab_game, line);
      stock(j, tab_game, arg);
      if (j % 11 == 10)
	{
	  if ((tab_game = realloc(tab_game, sizeof(char) * 122)) == NULL ||
	      (line = realloc(line, sizeof(char) * 21)) == NULL)
	    return (1);
	  tab_game = set_line_null(tab_game, 122);
	  line = set_line_null(tab_game,  21);
	}
    }
  if (j % 11 != 10)
    return (my_putstr_err("MAP ERROR\n\n"), 1);
  free(tab_game);
  free(line);
  return (0);
}

int	parse_tab(t_arglist *arg)
{
  char	*line;
  char	*tab_game;
  int	j;

  if ((tab_game = malloc(sizeof(char) * 122)) == NULL ||
      (line = malloc(sizeof(char) * 21)) == NULL)
    return (1);
  tab_game = set_line_null(tab_game, 122);
  line = set_line_null(tab_game, 21);
  j = -1;
  if ((read_the_file(arg, line, tab_game, j)) == 1)
    return (1);
  return (0);
}

int	main()
{
  t_arglist	arg;

  if (parse_tab(&arg) == 1)
    return (1);
  solve(&arg);
  free_list(&arg);
  return (0);
}
