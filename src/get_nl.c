/*
** get_next_line.c for get_next_line in /home/asphahyre/rendu/CPE_2015_getnextline
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Mon Jan 04 11:02:21 2016 Luka Boulagnon
** Last update Mon Jan 04 22:50:32 2016 Luka Boulagnon
*/

#include "get_next_line.h"

char	*biggify(char *before, int *sz)
{
  char	*new;
  int	size;

  *sz += READ_SIZE;
  new = malloc(*sz);
  size = *sz - READ_SIZE;
  while (size--)
    new[size] = before[size];
  free(before);
  return (new);
}

int	contains(char *line, char c)
{
  return ((*line == c) ? 1 : (*line) ? contains(line + 1, c) : 0);
}

void	bufferize(char *line, char *tmp, int *size, int total)
{
  int	i;

  i = -1;
  while (++i < total)
    if (line[i] == '\n' && !(line[i] = 0))
      break;
  *size = 0;
  while (++i < total)
    tmp[(*size)++] = line[i];
}

void	get_from_buffer(char **str, char *buffer, int nb_char)
{
  while (--nb_char >= 0)
    (*str)[nb_char] = buffer[nb_char];
}

char		*get_next_line(const int fd)
{
  int		result;
  int		set;
  char		*line;
  int		size;
  static char	tmp[READ_SIZE];
  static int	nb_char = 0;

  line = malloc(READ_SIZE + nb_char);
  size = READ_SIZE + nb_char;
  get_from_buffer(&line, tmp, nb_char);
  if (!line && (size = READ_SIZE) && !(line = malloc(READ_SIZE)))
    return (NULL);
  set = 0;
  while ((result = read(fd, line + (size - READ_SIZE), READ_SIZE)) == READ_SIZE
	 && !contains(line, '\n') && (set = 1))
    line = biggify(line, &size);
  if (result == -1 || (!set && !result && !nb_char))
    return (NULL);
  bufferize(line, tmp, &nb_char, size - READ_SIZE + result);
  return (line);
}
