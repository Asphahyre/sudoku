/*
** solver.c for sudoki-bi in /home/asphahyre/rendu/sudoki-bi/src
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sat Feb 27 13:40:24 2016 Luka Boulagnon
** Last update Sun Feb 28 08:15:55 2016 Luka Boulagnon
*/

#include "sudo.h"

int		is_in_line(int **grid, int position)
{
  int		pos;

  pos = position - position % 9;
  while (pos < (position - position % 9 + 9))
    {
      if (get_value(grid, pos) && pos != position &&
	  (get_value(grid, pos) == get_value(grid, position)))
	return (1);
      ++pos;
    }
  return (0);
}

int		is_in_square(int **grid, int position)
{
  int		pos;
  int		x;
  int		y;

  pos = position - position % 3;
  while ((pos > 6 && pos < 27)
	 || (pos > 33 && pos < 54)
	 || (pos > 60))
    pos -= 9;
  x = 0;
  while (x++ < 3)
    {
      y = 0;
      while (y++ < 3)
	{
	  if (get_value(grid, pos) && pos != position &&
	      (get_value(grid, pos) == get_value(grid, position)))
	    return (1);
	  ++pos;
	}
      pos += 6;
    }
  return (0);
}

int		is_in_column(int **grid, int position)
{
  int		pos;

  pos = position % 9;
  while (pos < 81)
    {
      if (get_value(grid, pos) && pos != position &&
	  (get_value(grid, pos) == get_value(grid, position)))
	return (1);
      pos += 9;
    }
  return (0);
}

int		backtrack(int **grid, int *order)
{
  int		i;

  i = 80;
  while (i >= 0 && i < 81)
  {
    if (select_next(grid + order[i]))
      ++i;
    else if (!conflicts(grid, order[i]))
      --i;
  }
  return (i > 80);
}

int		solve(t_arglist *args)
{
  t_arglist	grids;
  t_cdlist	*grid;
  int		**filled;
  int		order[81];

  bzero(&order, sizeof(order));
  grids = *args;
  grid = grids.root;
  while (grids.length--)
    {
      if (!(filled = solve_grid(grid)))
	return (1);
      build_order(filled, order);
      if (backtrack(filled, order))
	print_invalid();
      else
	print_grid(filled);
      grid = grid->next;
      if (grids.length)
	printf("####################\n");
      free_grid(filled);
    }
  return (0);
}
