/*
** pruning.c for sudoki-bi in /home/asphahyre/rendu/sudoki-bi/src/solver
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 28 08:13:33 2016 Luka Boulagnon
** Last update Sun Feb 28 08:18:15 2016 Luka Boulagnon
*/

#include "sudo.h"

int		nb_bits(int nb)
{
   nb = nb - ((nb >> 1) & 0x55555555);
   nb = (nb & 0x33333333) + ((nb >> 2) & 0x33333333);
   return ((((nb + (nb >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24);
}

void		fill_bitfields(t_remains *remains, t_cdlist *grid, int i)
{
  remains->line = get_line(grid, i);
  remains->square = get_square(grid, i);
  remains->columns = get_column(grid, i);
  remains->total = remains->line & remains->square & remains->columns;
}

int		**solve_grid(t_cdlist *grid)
{
  int		i;
  int		j;
  int		nb;
  int		**possibilities;
  t_remains	remains;

  if (!(possibilities = malloc((82 + (i = -1)) * sizeof(int *))))
    exit(2);
  while (++i < 81)
    {
      if (!grid->data->tab[i])
	fill_bitfields(&remains, grid, i);
      else
	remains.total = 1 << (grid->data->tab[i] - 1);
      if (!(nb = nb_bits(remains.total)))
	return (NULL);
      if (!(possibilities[i] = malloc((nb + 2) * sizeof(int))))
	exit(2);
      bzero(possibilities[i]++, (nb + 2) * sizeof(int));
      j = 9;
      while (j--)
	if (((remains.total >> j) & 1))
	  possibilities[i][--nb] = j + 1;
    }
  return (possibilities);
}
