/*
** display.c for sudoki-bi in /home/asphahyre/rendu/sudoki-bi/src/solver
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 28 08:09:50 2016 Luka Boulagnon
** Last update Sun Feb 28 08:10:12 2016 Luka Boulagnon
*/

#include "sudo.h"

void		print_grid(int **grid)
{
  int		i;

  i = -1;
  printf("|------------------|\n|");
  while (++i < 81)
  {
    printf(" %d", get_value(grid, i));
    if (!((i + 1) % 9))
      printf("|\n|");
  }
  printf("------------------|\n");
}

void		print_invalid()
{
  int		i;

  i = -1;
  printf("|------------------|\n");
  while (++i < 9)
    printf("| X X X X X X X X X|\n");
  printf("|------------------|\n");
}

