/*
** pruning_parser.c for sudoki-bi in /home/asphahyre/rendu/sudoki-bi/src/solver
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 28 08:14:40 2016 Luka Boulagnon
** Last update Sun Feb 28 08:16:04 2016 Luka Boulagnon
*/

#include "sudo.h"

unsigned int	get_line(t_cdlist *grid, int position)
{
  int		return_nb;
  int		pos;

  return_nb = 0;
  pos = position - position % 9;
  while (pos < (position - position % 9 + 9))
    {
      if (grid->data->tab[pos])
	return_nb += 1 << (grid->data->tab[pos] - 1);
      ++pos;
    }
  return (~return_nb);
}

unsigned int	get_square(t_cdlist *grid, int position)
{
  int		return_nb;
  int		pos;
  int		x;
  int		y;

  return_nb = 0;
  pos = position - position % 3;
  while ((pos > 6 && pos < 27)
	 || (pos > 33 && pos < 54)
	 || (pos > 60))
    pos -= 9;
  x = 0;
  while (x++ < 3)
    {
      y = 0;
      while (y++ < 3)
	{
	  if (grid->data->tab[pos])
	    return_nb += 1 << (grid->data->tab[pos] - 1);
	  ++pos;
	}
      pos += 6;
    }
  return (~return_nb);
}

unsigned int	get_column(t_cdlist *grid, int position)
{
  int		return_nb;
  int		pos;

  return_nb = 0;
  pos = position % 9;
  while (pos < 81)
    {
      if (grid->data->tab[pos])
	return_nb += 1 << (grid->data->tab[pos] - 1);
      pos += 9;
    }
  return (~return_nb);
}

int		get_value(int **grid, int pos)
{
  return (grid[pos][*(grid[pos] - 1) - 1]);
}
