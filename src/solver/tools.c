/*
** tools.c for sudoki-bi in /home/asphahyre/rendu/sudoki-bi/src/solver
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 28 08:11:13 2016 Luka Boulagnon
** Last update Sun Feb 28 08:12:32 2016 Luka Boulagnon
*/

#include "sudo.h"

int		nb_possibilities(int *array)
{
  return (*array ? nb_possibilities(array + 1) + 1 : 0);
}

void		build_order(int **grid, int *order)
{
  int		level;
  int		pos;
  int		i;

  level = 10;
  pos = 0;
  while (--level && (i = 81))
    while (i--)
      if (nb_possibilities(grid[i]) == level)
	order[pos++] = i;
}

void		free_grid(int **filled)
{
  int		i;

  i = 81;
  while (i--)
    free(filled[i] - 1);
  free(filled);
}

int		conflicts(int **grid, int pos)
{
  return (is_in_line(grid, pos)
	  || is_in_column(grid, pos)
	  || is_in_square(grid, pos));
}

int		select_next(int **frame)
{
  int		pos;

  pos = 0;
  while ((*frame)[pos++]);
  if (++(*(*frame - 1)) >= pos)
    {
      (*(*frame - 1)) = 0;
      return (1);
    }
  return (0);
}
