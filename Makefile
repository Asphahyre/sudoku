##
## Makefile for SUDOKU in /home/bougon_p/rendu/sudoki-bi
## 
## Made by bougon_p
## Login   <bougon_p@epitech.net>
## 
## Started on  Fri Feb 26 21:39:48 2016 bougon_p
## Last update Sun Feb 28 08:41:53 2016 Luka Boulagnon
##

# USEFUL VARIABLES

OPTI	=	no

RM      =	rm -rf

GREEN	=	\033[1;32m

WHITE	=	\033[0m

ECHO	=	echo -e


# SOURCES VARIABLES

MAIN		=	src/

FILES		=	sudoku.c \
			get_nl.c \
			set_line.c \
			err.c \
			tools.c \
			debug.c \
			initlist.c \
			solver/solver.c \
			solver/pruning_parser.c \
			solver/pruning.c \
			solver/tools.c \
			solver/display.c

SRC 		=	$(addprefix $(MAIN),$(FILES))

OBJS    	=	$(SRC:.c=.o)


# LIBRARY VARIABLES



# PROJECT VARIABLES

NAME	=	sudoki

IFLAG	=	-Iinclude/

CFLAGS  =	-W -Wall -Wextra $(IFLAG) -g

ifeq ($(OPTI), yes)
	CFLAGS	+=	-O3
endif

CC      =	gcc $(CFLAGS)


# PROJECT RULES

anyway		:
			make -j 4 all

warning 	:
			@$(ECHO)
ifeq ($(OPTI), yes)
			@$(ECHO) "[31m  ! WARNING ![0m"
			@$(ECHO) "[33m You're building the project with the gcc's -O3 flag, which is supported for the [1m$(NAME)[0;33m; it will divide the program's duration by... around... a lot![0m"
			@$(ECHO)
endif

$(NAME)		: 	$(OBJS)
			@$(ECHO) "$(GREEN)\n> Compiling project\t >>>>>>>>>>>>>>> \t DONE\n$(WHITE)"
			@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

all		:	warning $(NAME)

clean		:
			@$(RM) $(OBJS)
			@$(ECHO) "$(GREEN)\n> Cleaning repository\t >>>>>>>>>>>>>>> \t DONE\n$(WHITE)"

fclean		: 	clean
			@$(RM) $(NAME)
			@$(ECHO) "$(GREEN)\n> Cleaning exec\t\t >>>>>>>>>>>>>>> \t DONE\n$(WHITE)"

re		:	fclean all

.c.o		:
			@$(CC) -c $< -o $@
			@$(ECHO) "$(GREEN)[OK] > $<\t \t $(WHITE)"

opti 		:
ifeq ($(OPTI), no)
			@$(ECHO)
			@$(ECHO) "[31m  ! WARNING ![0m"
			@$(ECHO) "[33m You're building the project with the gcc's -O3 flag, which is supported for the [1m$(NAME)[0;33m; it will divide the program's duration by... around... a lot![0m"
			@$(ECHO)
endif
			sed -i 's/^OPTI	=	no/OPTI	=	off/' Makefile
			sed -i 's/^OPTI	=	yes/OPTI	=	no/' Makefile
			sed -i 's/^OPTI	=	off/OPTI	=	yes/' Makefile

.PHONY		:	anyway all clean fclean re opti echo
