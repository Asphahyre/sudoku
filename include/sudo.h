/*
** sudo.h for SUDOKU in /home/bougon_p/rendu/sudoki-bi
**
** Made by bougon_p
** Login   <bougon_p@epitech.net>
**
** Started on  Fri Feb 26 21:45:59 2016 bougon_p
** Last update Sun Feb 28 08:20:03 2016 Luka Boulagnon
*/

#ifndef SUDO_H_
# define SUDO_H_

# include "get_next_line.h"
# include "list.h"

# include <string.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

typedef struct		s_remains
{
  unsigned short	line : 9;
  unsigned short	square : 9;
  unsigned short	columns : 9;
  unsigned short	total : 9;
}			t_remains;

char		*clean_str(char *);
char		*get_next_line(const int);
char		*set_line_null(char *, int);
int		**solve_grid(t_cdlist *);
int		backtrack(int **, int *);
int		check_line(char *, int);
int		conflicts(int **, int);
int		get_value(int **, int);
int		is_in_column(int **, int);
int		is_in_line(int **, int);
int		is_in_square(int **, int);
int		nb_bits(int);
int		nb_possibilities(int *);
int		select_next(int **);
int		solve(t_arglist *);
unsigned int	get_column(t_cdlist *, int);
unsigned int	get_line(t_cdlist *, int);
unsigned int	get_square(t_cdlist *, int);
void		build_order(int **, int *);
void		fill_bitfields(t_remains *, t_cdlist *, int);
void		free_grid(int **);
void		my_putstr_err(char *);
void		print_debug(t_arglist *);
void		print_grid(int **);
void		print_invalid();
void		sove(t_arglist *);

#endif /* !SUDO_H_ */
